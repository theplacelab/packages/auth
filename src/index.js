import ValidateComponent from "./components/Validate";
import SignInComponent from "./components/SignIn";
import React from "react";
import { Provider } from "react-redux";
import store from "./store.js";
import * as actions from "./actions";

const Validate = (props) => {
  return (
    <Provider store={store}>
      <ValidateComponent {...props} />
    </Provider>
  );
};

const SignIn = (props) => {
  return (
    <Provider store={store}>
      <SignInComponent {...props} />
    </Provider>
  );
};

const openSignInModal = (prefill) => {
  store.dispatch({
    type: actions.SET_SIGNIN_MODAL,
    payload: { modalVisible: true, prefill }
  });
};

const closeSignInModal = () => {
  store.dispatch({
    type: actions.SET_SIGNIN_MODAL,
    payload: { modalVisible: false, prefill: null }
  });
};

export { SignIn, Validate, openSignInModal, closeSignInModal };
