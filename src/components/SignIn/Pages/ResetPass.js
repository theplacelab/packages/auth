import React, { createContext, useContext } from "react";
import { FIELDTYPE, FormElement } from "@theplacelab/ui";
import { SignInContext } from "./index.js";

export const ResetPass = () => {
  const context = useContext(SignInContext);
  const {
    styles,
    onChange,
    onContinue,
    formValues,
    helpLink,
    badPassLimit,
    passwordTries,
    isWorking,
    onGoBack,
    onReset,
  } = context;

  return (
    <form onSubmit={onContinue}>
      <div style={styles.container}>
        <div style={styles.title}>
          Welcome back {formValues.submitted_email}!
        </div>
        <div style={styles.goBack} onClick={onGoBack}>
          Not your email?{" "}
          <span style={{ textDecoration: "underline" }}>Go Back</span>.
        </div>
        <div style={styles.subtitle}>
          Please update your password to continue
        </div>
        <div style={styles.row}>
          <div style={styles.col}>
            <div
              style={
                badPassLimit < passwordTries && !isWorking
                  ? styles.label_bad
                  : styles.label
              }
            >
              Current Password:
            </div>
            <div>
              <FormElement
                id={"password"}
                type={FIELDTYPE.PASSWORD}
                onChange={onChange}
              />
            </div>
            <div>New Password:</div>
            <div>
              <FormElement
                id={"new_password"}
                type={FIELDTYPE.PASSWORD}
                onChange={onChange}
              />
            </div>
            <div>New Password Again:</div>
            <div>
              <FormElement
                id={"new_password_confirm"}
                type={FIELDTYPE.PASSWORD}
                onChange={onChange}
              />
            </div>
          </div>
        </div>
        <div style={{ ...styles.row, marginTop: "1rem" }}>
          <div style={styles.label}>Remember Me:</div>
          <div>
            <FormElement
              id={"extendedSession"}
              value={formValues["extendedSession"]}
              type={FIELDTYPE.CHECKBOX}
              onChange={onChange}
            />
          </div>
        </div>
        <div style={styles.row}>
          <button style={styles.button} type="submit">
            Sign In
          </button>
        </div>
        {badPassLimit < passwordTries && (
          <div>
            <a onClick={onReset}>reset account</a>
          </div>
        )}
        <div style={styles.row}>{helpLink}</div>
      </div>
    </form>
  );
};
