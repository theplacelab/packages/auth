import React, { useContext } from "react";
import { FIELDTYPE, FormElement } from "@theplacelab/ui";
import { SignInContext } from "./index.js";

export const AskForEmail = () => {
  const context = useContext(SignInContext);
  const {
    styles,
    onChange,
    onContinue,
    formValues,
    helpLink,
    isWorking,
    hasCloseButton,
  } = context;

  return (
    <form onSubmit={onContinue}>
      <div style={styles.container}>
        <div style={styles.title}>Sign In To Continue</div>
        <div style={styles.subtitle}>
          Sign in or register with your email address
        </div>
        <div style={styles.row}>
          <FormElement
            label={"Email Address:"}
            style={styles}
            id={"email"}
            type={FIELDTYPE.EMAIL}
            onChange={onChange}
          />
        </div>
        <div style={styles.row}>
          <button style={styles.button} type="submit">
            Continue
          </button>
        </div>
        <div style={styles.row}>{helpLink}</div>
      </div>
    </form>
  );
};
