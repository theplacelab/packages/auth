import React, { useContext } from "react";
import { SignInContext } from "./index.js";
import { useDispatch } from "react-redux";
import { userLogout } from "/src/actions.js";

export const SignOut = () => {
  const context = useContext(SignInContext);
  const dispatch = useDispatch();
  const { styles, onCloseModal, helpLink } = context;

  return (
    <div style={styles.container}>
      <div style={styles.title}>Sign Out</div>
      <div style={styles.subtitle}>Do you really want to sign out?</div>

      <div style={styles.row}>
        <div style={{ marginRight: "1rem" }}>
          <button
            onClick={() => dispatch(userLogout())}
            style={styles.button}
            type="submit"
          >
            Sign Out
          </button>
        </div>
        <div>
          <button onClick={onCloseModal} style={styles.button} type="submit">
            Cancel
          </button>
        </div>
      </div>
      <div style={styles.row}>{helpLink}</div>
    </div>
  );
};
