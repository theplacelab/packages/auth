import React, { createContext, useContext } from "react";
import { FIELDTYPE, FormElement } from "@theplacelab/ui";
import { SignInContext } from "./index.js";

export const ConfirmNewAccount = () => {
  const context = useContext(SignInContext);
  const { onCloseModal } = context;

  return (
    <div style={{ display: "flex" }}>
      <div style={{ display: "flex", flexDirection: "column", height: "100%" }}>
        <div>Your account has been created!</div>
        <div>Check your email to complete registration.</div>
        <div>
          <input type="button" value="Ok" onClick={onCloseModal}></input>
        </div>
      </div>
    </div>
  );
};
