import React, { createContext, useContext } from "react";
import { FIELDTYPE, FormElement } from "@theplacelab/ui";
import { SignInContext } from "./index.js";

export const NewSignup = () => {
  const context = useContext(SignInContext);
  const { styles, onChange, onContinue, formValues, onGoBack } = context;

  return (
    <form onSubmit={onContinue}>
      <div style={styles.container}>
        <div style={styles.title}>Create your account</div>
        <div style={styles.goBack} onClick={onGoBack}>
          Not your email?{" "}
          <span style={{ textDecoration: "underline" }}>Go Back</span>.
        </div>
        <div style={styles.row}>
          <div style={styles.label}>Email:</div>
          <div>{formValues.submitted_email}</div>
        </div>
        <div style={styles.row}>
          <div style={styles.label}>Name:</div>
          <div>
            <FormElement
              id={"name"}
              type={FIELDTYPE.TEXT}
              onChange={onChange}
            />
          </div>
        </div>
        <div style={{ ...styles.row, marginTop: "2rem" }}>
          <button style={styles.button} type="submit">
            Register
          </button>
        </div>
      </div>
    </form>
  );
};
