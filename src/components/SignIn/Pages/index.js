import React, { useState, createContext, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  usernameAvailable,
  userLogin,
  userCreate,
  userRequestValidation,
  userLoginAndChangePassword
} from "/src/actions.js";
import { Spinner } from "/src/components/Spinner";
import { NewSignup } from "./NewSignup";
import { AskForEmail } from "./AskForEmail";
import { SignOut } from "./SignOut";
import { WelcomeBack } from "./WelcomeBack";
import { AccountInvalid } from "./AccountInvalid";
import { ResetPass } from "./ResetPass";
import { ConfirmNewAccount } from "./ConfirmNewAccount";

export const SignInContext = createContext();

export const Pages = ({
  helpLink,
  onCloseModal,
  prefill,
  onStatus,
  style = {},
  isAuthenticated,
  hasCloseButton
}) => {
  const styles = {
    overlay: {
      position: "absolute",
      backgroundColor: "white",
      height: "100%",
      width: "100%",
      zIndex: 1,
      opacity: 1.0,
      display: "flex"
    },
    goBack: {
      margin: "1rem",
      opacity: "0.5"
    },
    container: {
      textAlign: "center",
      padding: "0rem",
      margin: "auto",
      position: "absolute",
      bottom: 0,
      top: 0,
      left: 0,
      right: 0,
      ...style.container
    },
    label: {
      fontWeight: "900",
      marginRight: ".2rem",
      ...style.label
    },
    label_bad: {
      color: "red",
      fontWeight: "900",
      ...style.label_bad
    },
    row: {
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center"
    },
    title: {
      fontSize: "1.5rem",
      margin: "0 0 1rem 0",
      ...style.title
    },
    subtitle: {
      fontSize: "1rem",
      margin: "0 0 1rem 0",
      ...style.subtitle
    },
    button: {
      height: "2rem",
      border: "1px solid grey",
      borderRadius: "0.3rem",
      ...style.button
    }
  };
  const dispatch = useDispatch();
  const isWorking = useSelector((redux) => redux.isWorking);
  const availableUsername = useSelector((redux) => redux.availableUsername);
  const [formValues, setFormValues] = useState({
    extendedSession: true
  });

  // How many password tries allowed
  const passwordTries = 3;
  const [badPassLimit, setBadPassLimit] = useState(passwordTries);
  if (badPassLimit === 0) {
    setFormValues({});
    setBadPassLimit(passwordTries);
  }

  // Called indirectly via useEffect to avoid "update while render" hook issue
  let statusMessage;
  const onPostStatus = (message) => {
    if (typeof onStatus === "function")
      onStatus({
        icon: "envelope",
        backgroundColor: "#4c6d89",
        message
      });
  };
  useEffect(() => {
    if (statusMessage && typeof onStatus === "function") {
      onPostStatus(statusMessage);
      onCloseModal();
    }
  });

  // Either prefill OR lookup
  const [accountExists, setAccountExists] = useState(false);
  const [accountValid, setAccountValid] = useState(false);
  const [accountNeedsReset, setAccountNeedsReset] = useState(false);
  useEffect(() => {
    if (availableUsername) {
      setAccountExists(!availableUsername.available);
      setAccountValid(availableUsername.valid);
      setAccountNeedsReset(availableUsername.reset);
    }
  }, [availableUsername]);
  useEffect(() => {
    if (prefill) {
      setFormValues({ email: prefill.email, submitted_email: prefill.email });
      setAccountExists(!prefill.available);
      setAccountValid(prefill.v);
      setAccountNeedsReset(prefill.r);
    }
  }, [prefill]);

  // Handles each page
  const [loginStage, setLoginStage] = useState(0);
  const onContinue = (e) => {
    e?.preventDefault();
    const newFormValues = {
      ...formValues,
      submitted_email: formValues["email"],
      submitted_password: formValues["password"],
      submitted_name: formValues["name"]
    };
    setFormValues(newFormValues);
    if (
      newFormValues.submitted_email?.length > 0 &&
      newFormValues.submitted_password?.length > 0
    ) {
      setBadPassLimit(badPassLimit - 1);
      setFormValues({ ...newFormValues, submitted_password: "" });
      if (accountNeedsReset) {
        if (
          formValues.new_password &&
          formValues.new_password_confirm === formValues.new_password
        ) {
          dispatch(
            userLoginAndChangePassword({
              username: newFormValues.submitted_email,
              password: newFormValues.submitted_password,
              newpass: formValues.new_password,
              extendedSession: newFormValues.extendedSession
            })
          );
        } else {
          console.error("New passwords don't match");
        }
      } else {
        dispatch(
          userLogin({
            username: newFormValues.submitted_email,
            password: newFormValues.submitted_password,
            extendedSession: newFormValues.extendedSession
          })
        );
      }
    } else if (
      !accountExists &&
      newFormValues.submitted_email &&
      newFormValues.submitted_name
    ) {
      dispatch(
        userCreate({
          username: newFormValues.submitted_email,
          name: newFormValues.submitted_name
        })
      );
    } else if (newFormValues.submitted_email?.length > 0) {
      dispatch(usernameAvailable({ username: newFormValues.submitted_email }));
    }
    setLoginStage(loginStage + 1);
  };

  const onChange = (payload) => {
    setFormValues({ ...formValues, [payload.id]: payload.value });
  };

  const onRevalidate = (payload) => {
    onCloseModal();
    dispatch(userRequestValidation({ username: formValues.submitted_email }));
    onPostStatus(
      "Validation re-sent, check your inbox to complete registration!"
    );
  };

  const onReset = (payload) => {
    onCloseModal();
    dispatch(userRequestValidation({ username: formValues.submitted_email }));
    onPostStatus("Account reset, check your inbox to log back in");
  };

  let pageContent = isAuthenticated ? <SignOut /> : <AskForEmail />;
  if (
    !accountExists &&
    formValues.submitted_email &&
    formValues.submitted_name
  ) {
    pageContent = <ConfirmNewAccount />;
    statusMessage =
      "Account created, check your inbox to complete registration!";
  } else if (formValues.submitted_email && !isWorking) {
    if (accountExists) {
      if (!accountValid) {
        pageContent = <AccountInvalid />;
      } else if (accountNeedsReset) {
        pageContent = <ResetPass />;
      } else {
        pageContent = <WelcomeBack />;
      }
    } else {
      pageContent = <NewSignup />;
    }
  }

  return (
    <SignInContext.Provider
      value={{
        styles,
        onReset,
        onChange,
        onContinue,
        onRevalidate,
        onGoBack: () => setFormValues({}),
        formValues,
        helpLink,
        badPassLimit,
        passwordTries,
        onCloseModal,
        isWorking,
        onStatus,
        hasCloseButton
      }}
    >
      <Spinner>{pageContent}</Spinner>
    </SignInContext.Provider>
  );
};

export default Pages;
