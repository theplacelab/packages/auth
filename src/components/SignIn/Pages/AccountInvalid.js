import React, { useContext } from "react";
import { SignInContext } from "./index.js";

export const AccountInvalid = () => {
  const context = useContext(SignInContext);
  const { styles, onCloseModal, onRevalidate, onGoBack } = context;

  return (
    <div style={styles.container}>
      <div style={styles.title}>Validate Your Account</div>
      <div style={styles.goBack} onClick={onGoBack}>
        Not your email?{" "}
        <span style={{ textDecoration: "underline" }}>Go Back</span>.
      </div>
      <div style={styles.subtitle}>
        To use this account you must validate it by clicking the link which has
        been emailed to you. If you have not received your validation email,
        please check your spam folder.
      </div>
      <div style={{ ...styles.row, marginTop: "1rem" }}>
        <input
          style={styles.button}
          type="button"
          value="Re-Send Validation Email"
          onClick={onRevalidate}
        />
      </div>
    </div>
  );
};
