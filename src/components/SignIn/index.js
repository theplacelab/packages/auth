import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { userLoginConfig, setModalVisible } from "/src/actions.js";
import Pages from "./Pages";
import { Modal } from "@theplacelab/ui";
import { Ring } from "react-spinners-css";

const SignIn = ({
  authService,
  authHelper,
  onLogin,
  onLogout,
  onStatus,
  onAvatarClick,
  helpLink,
  style,
  prefill,
  showUsername = false,
  sizeThreshold = 900,
  hasCloseButton = true,
  loginText = "Sign In",
  logoutText = "Sign Out "
}) => {
  const windowSize = () => {
    const [windowSize, setWindowSize] = useState({
      width: undefined,
      height: undefined,
      sizeThreshold: false
    });
    useEffect(() => {
      function handleResize() {
        setWindowSize({
          width: window.innerWidth,
          height: window.innerHeight,
          threshold: window.innerWidth < sizeThreshold
        });
      }
      window.addEventListener("resize", handleResize);
      handleResize();
      return () => window.removeEventListener("resize", handleResize);
    }, []);
    return windowSize;
  };
  const size = windowSize();
  const authServiceReachable = useSelector(
    (redux) => redux.authServiceReachable
  );
  const isAuthenticated = useSelector((redux) => redux.isAuthenticated);
  const isInitializing = useSelector((redux) => redux.isInitializing);
  const modalVisible = useSelector((redux) => redux.modalVisible);
  const prefillRedux = useSelector((redux) => redux.prefill);

  const pfill = prefillRedux ? prefillRedux : prefill;

  const dispatch = useDispatch();
  const email = useSelector((redux) => redux.claims?.email);

  // Configure services
  useEffect(() => {
    dispatch(
      userLoginConfig({
        authService,
        authHelper,
        onLogin,
        onLogout,
        onAvatarClick
      })
    );
  }, [authService, authHelper, onLogin, onLogout]);

  useEffect(() => {
    dispatch(
      setModalVisible({
        modalVisible: pfill && !isAuthenticated ? true : false
      })
    );
  }, [isAuthenticated]);

  // Prevents FOUC before we stabilize
  if (isInitializing) return <Ring color="white" size={20} />;
  const styles = {
    ...style,
    container: {
      padding: 0,
      margin: 0,
      ...style?.container
    },
    title: {
      margin: 0,
      ...style?.title
    },
    subtitle: {
      margin: "1rem",
      ...style?.subtitle
    },
    footer: {
      display: "flex",
      textAlign: "center",
      background: "lightGrey",
      width: "100%",
      height: "4rem",
      position: "absolute",
      bottom: 0,
      ...style?.footer
    },
    modal: size.threshold
      ? {
          height: "35rem",
          width: "35rem",
          maxWidth: "100vh",
          maxHeight: "100vh",
          minWidth: "100vw",
          minHeight: "100vh"
        }
      : {
          padding: 0,
          display: "flex",
          height: "35rem",
          width: "35rem",
          marginTop: "10rem",
          maxWidth: "40rem",
          maxHeight: "30rem"
        }
  };

  if (!authServiceReachable) {
    return <div className={"fas fa-exclamation-triangle"} />;
  } else {
    return (
      <React.Fragment>
        {(!isAuthenticated && (
          <div
            style={{ cursor: "pointer" }}
            onClick={() => dispatch(setModalVisible({ modalVisible: true }))}
          >
            {loginText}
          </div>
        )) || (
          <div
            style={{ cursor: "pointer" }}
            onClick={() => dispatch(setModalVisible({ modalVisible: true }))}
          >
            {logoutText} {showUsername ? `(${email})` : ""}
          </div>
        )}
        {modalVisible && (
          <Modal
            onOutsideClick={() =>
              dispatch(setModalVisible({ modalVisible: false }))
            }
            style={styles.modal}
          >
            <Pages
              isAuthenticated={isAuthenticated}
              {...{
                hasCloseButton,
                onCloseModal: () =>
                  dispatch(setModalVisible({ modalVisible: false })),
                helpLink,
                style,
                prefill: pfill,
                onStatus
              }}
            />
            {hasCloseButton && (
              <div style={styles.footer}>
                <div style={{ margin: "auto" }}>
                  <input
                    style={styles.button}
                    type="button"
                    value={"Cancel"}
                    onClick={() =>
                      dispatch(setModalVisible({ modalVisible: false }))
                    }
                  />
                </div>
              </div>
            )}
          </Modal>
        )}
      </React.Fragment>
    );
  }
};

export default SignIn;
