import React from "react";
import { Ring } from "react-spinners-css";
import { useSelector } from "react-redux";
export const Spinner = ({ children }) => {
  const isWorking = useSelector((redux) => redux.isWorking);
  return (
    <React.Fragment>
      {isWorking && (
        <div
          style={{
            position: "absolute",
            backgroundColor: "white",
            height: "100%",
            width: "100%",
            zIndex: 1,
            opacity: 1.0,
            display: "flex",
          }}
        >
          <div style={{ width: "100%", margin: "auto", textAlign: "center" }}>
            <Ring color="#7a7676" size={32} />
          </div>
        </div>
      )}
      {children}
    </React.Fragment>
  );
};
