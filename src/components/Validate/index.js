import React, { useEffect, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { userValidate } from "/src/actions.js";
import { Ring } from "react-spinners-css";
import { userLoginConfig } from "/src/actions.js";

const Validate = ({
  token,
  onComplete,
  authService,
  authHelper,
  onLogin,
  onLogout,
  onAvatarClick
}) => {
  const dispatch = useDispatch();
  const isWorking = useSelector((redux) => redux.isWorking);
  const authServiceReachable = useSelector(
    (redux) => redux.authServiceReachable
  );
  const accountStatus = useSelector((redux) => redux.validatedAccount);
  const displayEl = useRef(null);
  // Configure services
  useEffect(() => {
    dispatch(
      userLoginConfig({
        authService,
        authHelper,
        onLogin,
        onLogout,
        onAvatarClick
      })
    );
  }, [authService, authHelper, onLogin, onLogout]);

  useEffect(() => {
    if (authServiceReachable) dispatch(userValidate(token));
  }, [authServiceReachable, token]);

  useEffect(() => {
    if (!isWorking && accountStatus?.v && typeof onComplete === "function") {
      onComplete(accountStatus);
    } else if (!isWorking && displayEl.current !== null) {
      setTimeout(() => {
        displayEl.current.innerText = "Validation link is invalid or expired.";
      }, 1000);
    }
  }, [isWorking, accountStatus]);

  if (!authServiceReachable) return null;

  if (isWorking) {
    return (
      <div style={{ textAlign: "center" }}>
        Validating...
        <br /> <Ring color="#7a7676" size={32} />
      </div>
    );
  } else {
    return (
      <div>
        {(accountStatus?.v && (
          <div>Account Validated! Please sign in to continue</div>
        )) || (
          <div>
            <div ref={displayEl} style={{ textAlign: "center" }} />
          </div>
        )}
      </div>
    );
  }
};
export default Validate;
