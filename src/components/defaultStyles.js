const styles = {
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",

    display: "flex",
    flexDirection: "row",
    padding: "0",
  },
  input: {
    width: "10rem",
    fontSize: "1rem",
    height: "2rem",
    border: "1px solid black",
    marginRight: ".1rem",
    borderRadius: "0.3rem",
  },
  link: {
    color: "purple",
    textDecoration: "underline",
  },
  userAvatar: {
    border: "2px solid ",
    borderRadius: "3rem",
    height: "2rem",
    width: "2rem",
    lineHeight: "2rem",
    overflow: "hidden",
    fontWeight: "900",
    backgroundColor: "#d14787",
    textAlign: "center",
  },
  userAvatarLoading: {
    border: "2px solid #eeeeee",
    borderRadius: "3rem",
    height: "2rem",
    width: "2rem",
    lineHeight: "2rem",
    overflow: "hidden",
    fontWeight: "900",
    backgroundColor: "#eeeeee",
    textAlign: "center",
  },
};
export default styles;
