function action(type, payload = {}) {
  return { type, payload };
}

export const SET_SIGNIN_MODAL = "SET_SIGNIN_MODAL";
export const setModalVisible = (payload) => action(SET_SIGNIN_MODAL, payload);

export const AUTH_API_UNREACHABLE = "AUTH_API_UNREACHABLE";
export const AUTH_API_REACHABLE = "AUTH_API_REACHABLE";
export const USER_LOGIN = "USER_LOGIN";
export const USER_LOGIN_WITH_CHANGEPASS = "USER_LOGIN_WITH_CHANGEPASS";
export const USER_LOGIN_RESPONSE = "USER_LOGIN_RESPONSE";
export const USER_AVAILABILITY_CHECK = "USER_AVAILABILITY_CHECK";
export const USER_AVAILABILITY_CHECK_RESPONSE =
  "USER_AVAILABILITY_CHECK_RESPONSE";
export const USER_LOGOUT = "USER_LOGOUT";
export const USER_REAUTH = "USER_REAUTH";
export const USER_CREATE = "USER_CREATE";
export const USER_LOGIN_CONFIG = "USER_LOGIN_CONFIG";
export const USER_LOGIN_CONFIG_COMPLETE = "USER_LOGIN_CONFIG_COMPLETE";
export const USER_REQUEST_VALIDATION = "USER_REQUEST_VALIDATION";
export const USER_VALIDATE = "USER_VALIDATE";
export const USER_VALIDATE_RESPONSE = "USER_VALIDATE_RESPONSE";

export const usernameAvailable = (payload) =>
  action(USER_AVAILABILITY_CHECK, payload);
export const userLoginConfig = (payload) => action(USER_LOGIN_CONFIG, payload);
export const userCreate = (payload) => action(USER_CREATE, payload);
export const userLogin = (payload) => action(USER_LOGIN, payload);
export const userLogout = () => action(USER_LOGOUT);
export const authUnreachable = () => action(AUTH_API_UNREACHABLE);
export const userRequestValidation = (payload) =>
  action(USER_REQUEST_VALIDATION, payload);
export const userValidate = (payload) => action(USER_VALIDATE, payload);
export const userLoginAndChangePassword = (payload) =>
  action(USER_LOGIN_WITH_CHANGEPASS, payload);
