import { takeLatest, all, call } from "redux-saga/effects";
import * as actions from "./actions";
import {
  periodicallyReauthorize,
  login,
  logout,
  reauthorize,
  usernameAvailable,
  userCreate,
  validateConfig,
  userRequestValidation,
  userValidate,
  changePassAndLogin
} from "./saga";
import createSagaMiddleware from "redux-saga";
import rootReducer from "./reducer";
import { createStore, applyMiddleware } from "redux";

function* rootSaga() {
  yield all([
    periodicallyReauthorize(),
    takeLatest(actions.USER_LOGIN, login),
    takeLatest(actions.USER_LOGIN_WITH_CHANGEPASS, changePassAndLogin),
    takeLatest(actions.USER_LOGOUT, logout),
    takeLatest(actions.USER_REAUTH, reauthorize),
    takeLatest(actions.USER_AVAILABILITY_CHECK, usernameAvailable),
    takeLatest(actions.USER_CREATE, userCreate),
    takeLatest(actions.USER_LOGIN_CONFIG, validateConfig),
    takeLatest(actions.USER_REQUEST_VALIDATION, userRequestValidation),
    takeLatest(actions.USER_VALIDATE, userValidate)
  ]);
}

const sagaMiddleware = createSagaMiddleware();
const store = () => {
  let store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
  sagaMiddleware.run(rootSaga);
  return store;
};

export default store();
