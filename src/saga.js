import { put, call, select, delay } from "redux-saga/effects";
import * as actions from "./actions.js";

const decodeToken = (token) => {
  try {
    var base64Url = token.split(".")[1];
    var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
    return JSON.parse(window.atob(base64));
  } catch {
    return null;
  }
};

export const periodicallyReauthorize = function* () {
  const currentToken = localStorage.getItem("refreshToken");
  if (currentToken) yield call(reauthorize);
  while (true) {
    const reduxState = yield select();
    if (reduxState.authServiceReachable)
      if (reduxState.isAuthenticated) {
        let now = Date.now() / 1000;
        let secondsUntilExpire = reduxState.claims.exp - now;
        if (secondsUntilExpire <= 120) {
          yield call(reauthorize);
        }
      } else {
        if (currentToken) yield call(reauthorize);
      }
    yield delay(60000);
  }
};

export const reauthorize = function* (action) {
  const reduxState = yield select();
  const refreshToken = localStorage.getItem("refreshToken");
  if (reduxState.authServiceReachable) {
    if (refreshToken) {
      const response = yield _makeRequest({
        method: "POST",
        url: `${reduxState.authService}/rpc/reauthorize`,
        body: { token: refreshToken }
      });
      if (response && response.ok) {
        const res = yield response.json();
        const authToken = res.auth;
        const refreshToken = res.refresh;
        localStorage.setItem("refreshToken", refreshToken);
        const payload = {
          ...res,
          isAuthenticated: true,
          claims: decodeToken(authToken)
        };
        yield put({
          type: actions.USER_LOGIN_RESPONSE,
          payload
        });
        if (typeof reduxState.onLogin === "function")
          reduxState.onLogin(payload);
      } else {
        localStorage.removeItem("refreshToken");
        console.warn("Reauthorization Failed");
        yield call(logout);
      }
    } else {
      yield logout();
    }
  }
};

export const changePassAndLogin = function* (action) {
  localStorage.removeItem("refreshToken");
  const reduxState = yield select();

  if (reduxState.authService) {
    const email = action.payload.username.toLowerCase().trim();
    const pass = action.payload.password.trim();
    const response = yield _makeRequest({
      method: "POST",
      url: `${reduxState.authService}/rpc/changepass`,
      body: {
        email,
        pass,
        newpass: action.payload.newpass,
        extend: action.payload.extendedSession
          ? action.payload.extendedSession
          : false
      }
    });

    if (response && response.ok) {
      const res = yield response.json();
      const authToken = res.auth;
      const refreshToken = res.refresh;
      localStorage.setItem("refreshToken", refreshToken);
      const payload = {
        isAuthenticated: true,
        authToken,
        claims: decodeToken(authToken)
      };
      yield put({
        type: actions.USER_LOGIN_RESPONSE,
        payload
      });
      if (typeof reduxState.onLogin === "function")
        yield reduxState.onLogin(payload);
    } else {
      yield call(logout);
    }
  }
};

export const login = function* (action) {
  localStorage.removeItem("refreshToken");
  const reduxState = yield select();

  if (reduxState.authService) {
    const email = action.payload.username.toLowerCase().trim();
    const pass = action.payload.password.trim();
    const response = yield _makeRequest({
      method: "POST",
      url: `${reduxState.authService}/rpc/login`,
      body: {
        email,
        pass,
        extend: action.payload.extendedSession
          ? action.payload.extendedSession
          : false
      }
    });

    if (response && response.ok) {
      const res = yield response.json();
      const authToken = res.auth;
      const refreshToken = res.refresh;
      localStorage.setItem("refreshToken", refreshToken);
      const payload = {
        ...res,
        isAuthenticated: true,
        claims: decodeToken(authToken)
      };
      yield put({
        type: actions.USER_LOGIN_RESPONSE,
        payload
      });
      if (typeof reduxState.onLogin === "function")
        yield reduxState.onLogin(payload);
    } else {
      yield call(logout);
    }
  }
};

export const logout = function* (action) {
  const reduxState = yield select();
  localStorage.removeItem("refreshToken");
  const payload = {
    isAuthenticated: false,
    authToken: "",
    claims: {}
  };
  yield put({
    type: actions.USER_LOGIN_RESPONSE,
    payload
  });
  if (typeof reduxState.onLogout === "function") reduxState.onLogout(payload);
};

export const userValidate = function* (action) {
  const token = action.payload;
  const reduxState = yield select();

  if (reduxState.authServiceReachable) {
    const decodedToken = decodeToken(token);
    if (decodedToken?.email) {
      console.log(`Posting to ${reduxState.authHelper}/user/validate`);
      const checkToken = yield _makeRequest({
        method: "POST",
        url: `${reduxState.authHelper}/user/validate`,
        body: { token }
      });
      if (!checkToken.ok) {
        yield put({
          type: actions.USER_VALIDATE_RESPONSE,
          payload: null
        });
      } else {
        const checkUser = yield _makeRequest({
          method: "POST",
          url: `${reduxState.authService}/rpc/checkusername`,
          body: { username: decodedToken.email }
        });
        const userState = yield checkUser.json();
        yield put({
          type: actions.USER_VALIDATE_RESPONSE,
          payload: { ...userState, email: decodedToken.email }
        });
      }
    } else {
      yield put({
        type: actions.USER_VALIDATE_RESPONSE,
        payload: null
      });
    }
  }
};

export const userRequestValidation = function* (action) {
  const reduxState = yield select();
  const email = action.payload.username.toLowerCase().trim();
  if (reduxState.authServiceReachable) {
    const response = yield _makeRequest({
      method: "POST",
      url: `${reduxState.authHelper}/user/validate/reset`,
      body: { email }
    });
  }
};

export const usernameAvailable = function* (action) {
  const reduxState = yield select();
  const username = action.payload.username.toLowerCase().trim();
  if (reduxState.authServiceReachable) {
    const response = yield _makeRequest({
      method: "POST",
      url: `${reduxState.authService}/rpc/checkusername`,
      body: { username }
    });

    let payload;
    if (response && response.ok) {
      const res = yield response.json();
      payload = {
        username,
        available: res.available,
        valid: res.v,
        reset: res.r
      };
    } else {
      payload = {
        username,
        available: false,
        valid: res.v,
        reset: res.r
      };
    }
    yield put({
      type: actions.USER_AVAILABILITY_CHECK_RESPONSE,
      payload
    });
  }
};

export const userCreate = function* (action) {
  const reduxState = yield select();
  const email = action.payload.username.toLowerCase().trim();
  const name = action.payload.name.trim();
  if (reduxState.authServiceReachable)
    yield _makeRequest({
      method: "POST",
      url: `${reduxState.authHelper}/user`,
      body: { email, name }
    });
};

export const validateConfig = function* (action) {
  if (!action.payload.authService || !action.payload.authHelper) {
    console.error(
      "@theplacelab/user: Missing config. You must provide both authService and authHelper via <Login/>"
    );
    yield put({ type: actions.AUTH_API_UNREACHABLE });
  } else {
    const checkAuthService = yield _makeRequest({
      method: "GET",
      url: action.payload.authService
    });
    const checkAuthHelper = yield _makeRequest({
      method: "GET",
      url: action.payload.authHelper
    });
    if (checkAuthService.ok && checkAuthHelper.ok)
      yield put({
        type: actions.USER_LOGIN_CONFIG_COMPLETE,
        payload: action.payload
      });
    yield put({
      type: actions.USER_REAUTH
    });
  }
};

const _makeRequest = function* ({ method, url, body }) {
  try {
    let request = {
      method: method,
      headers: {
        Accept: "application/json, application/xml, text/plain, text/html, *.*",
        "Content-Type": "application/json; charset=utf-8"
      },
      body: body
        ? typeof body === "string"
          ? body
          : JSON.stringify(body).replace(/\\u0000/g, "")
        : null
    };
    if (!body || method === "GET") delete request.body;
    return yield fetch(url, request);
  } catch (e) {
    yield put({ type: actions.AUTH_API_UNREACHABLE });
    console.error("FATAL: Cannot reach auth API endpoint(s)!");
    return e;
  }
};
