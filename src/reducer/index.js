import * as actions from "../actions";

const user = (
  state = {
    authServiceReachable: false,
    isInitializing: true,
    isAuthenticated: false,
    token: "",
    availableUsernames: [],
    validatedAccounts: [],
    modalVisible: false,
    prefill: null
  },
  action
) => {
  switch (action.type) {
    case actions.USER_AVAILABILITY_CHECK:
    case actions.USER_LOGIN:
    case actions.USER_VALIDATE:
    case actions.USER_LOGIN_WITH_CHANGEPASS:
      return {
        ...state,
        isWorking: true
      };
    case actions.AUTH_API_UNREACHABLE:
      return { ...state, isInitializing: false, authServiceReachable: false };
    case actions.AUTH_API_REACHABLE:
      return { ...state, isInitializing: false, authServiceReachable: true };
    case actions.USER_LOGIN_CONFIG_COMPLETE:
      return {
        ...state,
        authServiceReachable: true,
        ...action.payload
      };
    case actions.USER_AVAILABILITY_CHECK_RESPONSE:
      return {
        ...state,
        availableUsername: action.payload,
        isWorking: false
      };
    case actions.USER_LOGIN_RESPONSE:
      return {
        ...state,
        ...action.payload,
        isWorking: false,
        isInitializing: false
      };
    case actions.USER_VALIDATE_RESPONSE:
      return {
        ...state,
        validatedAccount: action.payload,
        isWorking: false
      };
    case actions.SET_SIGNIN_MODAL:
      return {
        ...state,
        prefill: action.payload.prefill,
        modalVisible: action.payload.modalVisible
      };
    default:
      return state;
  }
};

export default user;
